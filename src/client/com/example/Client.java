package client.com.example;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.Buffer;

/**
 * Created by RENT on 2017-06-20.
 */
public class Client {

    public static void main(String[] args) {

//        System.out.println("HI!");

        try{

            String toSend = "idz do domu!!";

            Socket socket = new Socket("10.0.0.102", 9000); // soket w protokole TCP
            final InputStream inputStream = socket.getInputStream();
            final OutputStream outputStream = socket.getOutputStream();

            final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            final DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
            dataOutputStream.writeBytes("HI\r");

            String s = null;
            int state = 1;
            while ((s = bufferedReader.readLine()) != null) {
                System.out.println(s);
                if(state == 1 && s.equalsIgnoreCase("HI")){
                    dataOutputStream.writeBytes("SEND\r");
                    state++;
                } else if (state == 2 && s.equalsIgnoreCase("OK")){
                    dataOutputStream.writeBytes("SIZE:" + toSend.getBytes().length + "\r");
                    state++;
                } else if (state == 3 && s.equalsIgnoreCase("OK")){
                    dataOutputStream.writeBytes(toSend);
                    dataOutputStream.writeBytes("\r\r");
                }
            }

            bufferedReader.close();
            dataOutputStream.close();


        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
