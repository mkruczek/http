package client.com.example;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by RENT on 2017-06-20.
 */
public class Server {

    public static void main(String[] args) {

        try {
            final ServerSocket serverSocket = new ServerSocket(9000);
            while (true) {
                final Socket accept = serverSocket.accept();
                final Thread thread = new Connection(accept);
                thread.start();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}


class Connection extends Thread {

    private Socket socket;
    String getString;

    public Connection(Socket accept) {
        socket = accept;
    }

    public void run() {
        final InputStream inputStream;

        try {
            inputStream = socket.getInputStream();
            final OutputStream outputStream = socket.getOutputStream();
            final DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
            final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            String s = null;
            int state = 0;
            while ((s = bufferedReader.readLine()) != null) {
                System.out.println(s);

                if(state == 0 && s.equalsIgnoreCase("HI")){
                    dataOutputStream.writeBytes("HI\r");
                    state++;
                } else if(state == 1 && s.equalsIgnoreCase("SEND")){
                    dataOutputStream.writeBytes("OK\r");
                    state++;
                } else if(state == 2 && (s.startsWith("SIZE:"))){
                    dataOutputStream.writeBytes("OK\r");
                    state++;
                } else if(state == 3 && s != null){
                    getString = s;
                    state++;
                } else if (state == 6){
                    dataOutputStream.writeBytes("Odebrałem dane : " + getString + "\r");
                } else if (state == 8){
                    System.out.println("dziękuję za współpracę");
                } else {
                    state ++;
                }

            }

            inputStream.close();
            outputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

